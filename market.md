#    Sunucu Marketi
*Sadece ana malzemeler satılmaktadır, Hazır halde satın almak için sunucu tüccarları ile anlaşmanız gerekiyor.*
>
**Not:** *Tüccarlar eşya satarken %10 Devlet vergisi vermektedir. Normal oyuncular ise %40 devlet vergisi vermektedir.*
>
**Uyarı:** *Parayı önceden atıp sonrada eşya droplamak vs. yasaktır. Farkedilirse yetkili tarafından eşyaya değer biçilir ve **Biçilen Değer + %40 Faiz** ödemek zorunda kalırsınız.*
>
---
Eşya Adı    |    Fiyatı
----------- |    -----------
Örnek Eşya   | 5₺