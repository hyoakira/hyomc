# HyoMC
## Proje Hakkında
*Bu sunucunun yapımına **7 Ekim 2018** tarihinde başlanmıştır. Sunucunun tek amacı arkadaşlarınızla eğlenebileceğiniz bir ortam sağlamak.*
Projenin Sahibi *Hyosuke*'dir.

Projenin Orjinal Sunucuları
--
- **S1** HyoMC Lite = *s1sunucu.ip*

### Kurallarımız
- `Sunucu içerisinde kişiye yönelik rahatsız edici küfürler yasaktır.`
- `Sunucu içerisinde tüm üyeler eşittir, hiçkimse birbirine saygısızlık yapamaz.`
- `Sohbette müstehcen konuların konuşulması yasaktır.`
- `Başka sunucuların reklamını yapmak yasaktır.`
- `Oyun içerisinde, tek kişiye  ikiden fazla üye ile takım olup saldırmak yasaktır.`
- `Sunucuda gerçek hayat hakkında konuşmak yasaktır. Çünkü oyunun akışını bozuyor.`
- `İtem droplama yaparak satış gerçekleştirmek yasaktır.`


> *Kuralları **okuyan** ve **anlayan** kişiler sunucuya girdiğinde `/kuralları-okudum` komutunu kullanarak **ekstra 50 oyun parası** kazanabilir.*


## Kullanım Politikası
- Sunucuya girdiğiniz andan itibaren kurallarımızı kabul etmiş olursunuz.

> Sunucu içerisinde ki herşey **oyun parası** ile satın alınmaktadır ve **gerçek türk lirası** ile sadece **oyun parası** satın alabilirsiniz.
**Oyun parasını, oyunu oynarakta kazanabilirsiniz. Ayrıca oyunda haksızlık olmaması için gerekli tüm düzenlemeler yapıldı.**

Oyun Parası | Fiyatı
------------ | -------------
500 | 10₺
250 | 7₺
100 | 5₺

*Kazandığım paralar ile sizin sayenizde karıya gidiyorum*
## İletişim

**Discord Hesap Adım:** *Hyosuke#8687*
>*Discord üzerinden ulaşamaz iseniz **hyosuke@yandex.com** adresine e posta atarak üç gün içerisinde yanıt alabilirsiniz.*

![Emilia](https://camo.githubusercontent.com/6218f7458093b758c7ab1ac8dd5884b699aa3089/68747470733a2f2f7777772e616e696d656c65722e6e65742f75706c6f61642f6d656469612f706f7374732f323031382d30392f32342f32613239636635313462666637653765313462643331626235363533346136625f313533373737353239312d732e6a7067 "I love Emilia :D")

## Proje  Sponsorları

>Sende böyle muhteşem bir projeye sponsor olmak istemez misin?
